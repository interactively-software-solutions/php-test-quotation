## PHP Developer Test

This test is for us to understand your level of backend OOPHP experience, and is not to be used as a representation of your frontend skills. To that end please feel free to use Bootstrap for the UI to allow you to spend more time on the parts that matter.

### Requirements

1. Use Laravel framework latest version
2. The project should be provided in a way that a developer can pick up the project and understand what&#39;s going on straight away with little effort.

### Provide:

While there is no specific time limit, please indicate the total time that you spent completing the task. We recommend spending no more than 2.5 hours on this task, and remember we value the quality of backend programming vs frontend design.

### Task

Create a 3-step quotation process that has the following steps (can be a single form if you want):

1. Let the user enter their details as listed below, and once completed they should advance on to the next step.

		- Name
		- Password
		- Email Address
		- Phone Number
		
2. Products to quote for, there are two different types of product, both that have pricing based on different factors. They are outlined below, and an example of each provided, feel free to set up with just these two products. So you can just hardcoded them in your frontend quote and just set the quantity/ period

	**Subscription**
	
	For example, a subscription to a teach course. The user can select what period of time they want to do this for selecting start &amp; end date. The price should be set per day, and the user charged for every day attended (not including weekends).

	**Goods**
	
	For example, a textbook, these are simply sold in quantity, and the user selects how many of each item they want to be able to select
	
3. The quote should be saved in the database along with relevant information that could later be used to retrieve the quote and send an email notification to admin email address: **ciprian.oprea@interactively.eu**




Total quotation for the everything selected, this should include:

1. User&details
2. Breakdown of costs by product
3. Unique quote id for that quote
4. Total cost for the whole quote